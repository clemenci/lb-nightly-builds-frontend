variables:
    RYE_HOME: ${CI_PROJECT_DIR}/.cache/rye
    PRE_COMMIT_HOME: ${CI_PROJECT_DIR}/.cache/pre-commit

default:
    image: registry.cern.ch/docker.io/library/python:latest
    before_script:
        - |
            # ensure rye
            export PATH=${RYE_HOME}/shims:${PATH}
            if ! which rye >&/dev/null ; then
                curl -L https://github.com/astral-sh/rye/releases/latest/download/rye-x86_64-linux.gz | gunzip > rye.tmp
                chmod a+x rye.tmp
                ./rye.tmp self install --yes --no-modify-path
                rm -f rye.tmp
                (
                    echo '[[sources]]'
                    echo 'name = "default"'
                    echo 'url = "https://lhcb-repository.web.cern.ch/repository/pypi/simple"'
                ) >> $(rye config --show-path)
            fi
            which rye
            rye --version
        - |
            # ensure pre-commit
            if ! which pre-commit >&/dev/null ; then
                rye tools install pre-commit
            fi
        - |
            # pull dependencies
            rye sync --no-lock
        - |
            # install pre-commit hooks
            pre-commit install --install-hooks
    cache:
        paths:
            - .cache

prepare-cache:
    script: "true"

pre-commit:
    needs: [prepare-cache]
    script:
        - git fetch origin ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME:-master}
        - pre-commit run --from-ref FETCH_HEAD --to-ref HEAD

build:
    needs: [prepare-cache]
    script:
        - rye sync --no-lock --no-dev
        # we are not really using the packages version yet
        - rye build --wheel
        # generate a list of dependencies from the lock file
        # excluding the project itself
        - rye list | grep -v file:// > dist/requirements.txt
    artifacts:
        paths:
            - dist
        expire_in: 15 days

build-docs:
    needs: [prepare-cache]
    script:
        - rye sync --no-lock
        - rye run sphinx-build docs docs/_build/html
    artifacts:
        paths:
            - docs/_build/html
        expire_in: 15 days

docker-image:
    only:
        - master
        - prod
        - temporary
    needs:
        - job: build
          artifacts: true
        - job: build-docs
          artifacts: true
    variables:
        IMAGE_DESTINATION: ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_NAME}:${CI_PIPELINE_IID}
    image:
        name: gcr.io/kaniko-project/executor:debug
        entrypoint: [""]
    before_script: []
    script:
        - /kaniko/executor
          --context $CI_PROJECT_DIR
          --build-arg project=${CI_PROJECT_PATH}
          --build-arg commit=${CI_COMMIT_SHA}
          --dockerfile $CI_PROJECT_DIR/Dockerfile
          --destination $IMAGE_DESTINATION
        - echo "Image pushed successfully to ${IMAGE_DESTINATION}"
    cache: []

docker-image-tag:
    image:
        name: gitlab-registry.cern.ch/ci-tools/docker-builder/tools
    variables:
        # No need to clone the repo
        GIT_STRATEGY: none
        IMAGE_DESTINATION: ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_NAME}:${CI_PIPELINE_IID}
    needs:
        - job: "docker-image"
          artifacts: false
    before_script: []
    script:
        - source /kaniko/auth.sh
        - crane tag "${IMAGE_DESTINATION}" "latest"
        - crane validate --remote "${IMAGE_DESTINATION%:*}:latest"
    cache: []
    only:
        - master
        - prod
        - temporary
