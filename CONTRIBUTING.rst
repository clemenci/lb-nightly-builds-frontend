Contributing Guidelines
=======================

User and developers are encouraged to report bugs and propose new features.

Both bug reports and new features proposals has to be created as
`Gitlab issues`_, if possible with a matching merge request.

After a merge request is approved and merged, the new version of the frontend is
manually deployed to the production OpenShift application to be made publicly
available.

.. _`Gitlab issues`: https://gitlab.cern.ch/lhcb-core/lb-nightly-builds-frontend/-/issues/
