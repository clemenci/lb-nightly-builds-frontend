*LHCb Nightly Builds Frontend* is a [Flask](https://palletsprojects.com/p/flask/)
project to display web summary pages of LHCb Nightly builds.

See
* [docs/setup.rst](docs/setup.rst) for running and development instructions
