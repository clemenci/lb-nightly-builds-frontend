.. LHCb Nightly Builds Frontend documentation master file, created by
   sphinx-quickstart on Mon Dec  2 10:03:57 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to LHCb Nightly Builds Frontend's documentation!
========================================================

`LHCb Nightly Builds Frontend <..>`_ is a Flask_ project to display web summary
pages of LHCb Nightly Builds.

Project sources are accessible on `CERN Gitlab
<https://gitlab.cern.ch/lhcb-core/lb-nightly-builds-frontend>`_. Feature
requests and bug reports should be submitted via the `Gitlab issue tracker
<https://gitlab.cern.ch/lhcb-core/lb-nightly-builds-frontend/-/issues/>`_.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   setup.rst
   CONTRIBUTING.rst


.. _Flask: https://flask.palletsprojects.com/
