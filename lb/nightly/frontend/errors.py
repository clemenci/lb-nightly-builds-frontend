class FileTooBig(RuntimeError):
    def __init__(self, name, size=None, max_size=None) -> None:
        super().__init__(name, size, max_size)
        self.name = name
        self.size = size
        self.max_size = max_size

    def __str__(self) -> str:
        msg = f"requested file '{self.name}' is too big"
        if self.size or self.max_size:
            msg += " ("
            if self.size:
                msg += f"size: {self.size}{', ' if self.max_size else ''}"
            if self.max_size:
                msg += f"max size: {self.max_size}"
            msg += ")"
        return msg
