from flask import render_template, url_for
from werkzeug.exceptions import NotFound

from .application import app


@app.route("/<flavour>/summary/")
def legacy_summary(flavour):
    new_url = url_for("main", flavour=flavour, _external=True)
    if flavour == "release":
        new_url = url_for("releases", _external=True)
    return render_template("redirect.html", new_url=new_url)


@app.route("/<flavour>/<slot>/build/<int:build_id>/")
def legacy_slot_summary(flavour, slot, build_id):
    return render_template(
        "redirect.html",
        new_url=url_for(
            "main", flavour=flavour, slot=slot, build_id=build_id, _external=True
        ),
    )


@app.route("/logs/checkout/<flavour>/<slot>/<int:build_id>/<project>/")
def legacy_checkout_log(flavour, slot, build_id, project):
    return render_template(
        "redirect.html",
        new_url=url_for(
            "checkout",
            flavour=flavour,
            slot=slot,
            build_id=build_id,
            project=project,
            _external=True,
        ),
    )


@app.route("/logs/<type>/<flavour>/<slot>/<int:build_id>/<platform>/<project>/")
def legacy_log(type, flavour, slot, build_id, project, platform):
    if type not in ("build", "tests"):
        raise NotFound()
    return render_template(
        "redirect.html",
        new_url=url_for(
            type,
            flavour=flavour,
            slot=slot,
            build_id=build_id,
            project=project,
            platform=platform,
            _external=True,
        ),
    )


@app.route("/<flavour>/compare/<slot_b>/<int:build_id_b>/<slot_a>/<int:build_id_a>")
def legacy_compare(flavour, slot_a, build_id_a, slot_b, build_id_b):
    return render_template(
        "redirect.html",
        new_url=url_for(
            "compare",
            a=f"{flavour}/{slot_a}/{build_id_a}",
            b=f"{flavour}/{slot_b}/{build_id_b}",
            _external=True,
        ),
    )
