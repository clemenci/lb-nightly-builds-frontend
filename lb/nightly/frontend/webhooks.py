import json

import requests
from flask import request
from werkzeug.exceptions import InternalServerError, Unauthorized

from .application import app


@app.route("/jenkins/", methods=["POST"])
def jenkins_hook():
    """
    If the request carries the right token, and the data looks like a MR message with
    "/ci-test" in it, we add the data to a special document in CouchDB (legacy), so
    that a Jenkins task can pick it up and trigger the actual builds.
    """
    # check if the client has the right token
    if request.headers.get("X-Gitlab-Token") != app.config.get("WEBHOOK_KEY"):
        raise Unauthorized()

    # check if the payload is a MR message with "/ci-test" in the text
    if request.json and "/ci-test" in request.json.get("object_attributes", {}).get(
        "note", ""
    ):
        # FIXME drop the legacy ci-test code
        if app.config["LEGACY_CI_TESTS"]:
            trigger_ci_test_legacy(request.json)
        else:
            trigger_ci_test(request.json)

        return "request queued"

    else:
        return "nothing to do"


def trigger_ci_test_legacy(data):
    from .models import legacy_db

    # get the CouchDB document to update (create if not there)
    db = legacy_db("nightly")
    doc = (
        db["ci-tests"]
        if "ci-tests" in db
        else db.create_document({"_id": "ci-tests", "type": "ci-tests", "requests": []})
    )

    # update and publish (up to 5 attempts)
    for _ in range(5):
        try:
            doc["requests"].append(data)
            doc.save()
            break
        except requests.HTTPError as err:
            if "conflict" not in str(err).lower():
                raise
            doc.fetch()
    else:
        raise InternalServerError("failed to update the database")


def trigger_ci_test(data):
    response = requests.post(
        f"{app.config['JENKINS_URL']}/buildByToken/buildWithParameters?job=start-ci-test",
        data={
            "token": app.config["JENKINS_TOKENS"]["start-slots"],
            "webhook_data": json.dumps(data),
        },
        verify=False,  # FIXME: remove this
    )
    if not response.ok:
        raise InternalServerError(
            f"failed to trigger start-ci-test job: {response.text}"
        )
