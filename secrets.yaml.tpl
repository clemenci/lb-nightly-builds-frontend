# Tamplate for a ~/private/secrets.yaml file to be used to configure the frontend
---
# Nighty builds system
couchdb:
  url: "http://<user>:<password>@localhost:5984/nightly-builds"
artifacts:
  uri: "http://localhost:8081/repository/nightly_builds_artifacts"
logs:
  uri: "http://localhost:8081/repository/nightly_builds_logs"
rabbitmq:
  url: "amqp://<user>:<password>@rabbitmq"
mysql:
  url: "db+mysql://<user>:<password>@mysqldb/celery_backend"
elasticsearch:
  uri: "http://<user>:<password>@elasticsearch:9200"

# Frontend
cern_oauth:
  id: development
  secret: development
secret_key: "development"
webhook_key: "development"
gitlab_token: "development"

# Legacy system
legacy:
  couchdb: "https://<user>:<password>@lhcb-couchdb.cern.ch"
  artifacts: "https://lhcb-nightlies-artifacts.web.cern.ch/lhcb-nightlies-artifacts"
  rabbitmq: "dev/dev"
